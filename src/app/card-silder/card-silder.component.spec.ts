import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSilderComponent } from './card-silder.component';

describe('CardSilderComponent', () => {
  let component: CardSilderComponent;
  let fixture: ComponentFixture<CardSilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
