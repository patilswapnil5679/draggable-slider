import { Component, OnInit, Inject } from '@angular/core';
import { gsap, TweenMax, TimelineMax, TweenLite, Linear } from 'gsap';
import { Draggable } from 'gsap/Draggable';
import { DOCUMENT } from '@angular/common';


@Component({
  selector: 'app-card-silder',
  templateUrl: './card-silder.component.html',
  styleUrls: ['./card-silder.component.scss']
})
export class CardSilderComponent implements OnInit {


  // new
  picker: any;
  cells: any;
  proxy: any;
  cellWidth: any;
  numCells: any;
  cellStep: any;
  wrapWidth: any;
  baseTl: any;
  animation: any;
  dragable: any;
 
  constructor(@Inject(DOCUMENT) private _document) {
   }
 
  ngOnInit(): void {
    gsap.registerPlugin(Draggable);
  }
 
  ngAfterViewInit() {
    
    //new 
    this.picker = this._document.querySelector(".picker");
    this.cells = this._document.querySelectorAll(".cell");
    this.proxy = this._document.createElement("div");

    this.cellWidth = 450;
    //this.rotationX = 90;

    this.numCells = this.cells.length;
    this.cellStep = 1 / this.numCells;
    this.wrapWidth = this.cellWidth * this.numCells;

    this.baseTl = new TimelineMax({ paused: true });
    this.animation = new TimelineMax({ repeat: -1, paused: true }).add(this.baseTl.tweenFromTo(1, 2))


    Draggable.create(this.proxy, {
      // allowContextMenu: true,  
      type: "x",
      trigger: this.picker,
      throwProps: true,
      onDrag: this.updateProgress,
      onThrowUpdate: this.updateProgress,
      snap: { 
        x: this.snapX
      },
      onThrowComplete: function(){
        console.log("onThrowComplete");
        //TODO: animation that inject selected card title
      }
    });

    TweenLite.set(this.picker, {
      //perspective: 1100,
      width: this.wrapWidth - this.cellWidth
    });


    for (var i = 0; i < this.cells.length; i++) {  
      this.initCell(this.cells[i], i);
    }


  }

  initCell(element, index) {
    console.log(index);
    TweenLite.set(element, {
      width: this.cellWidth,
      scale: 0.6,
      //rotationX: rotationX,
      x: -(this.cellWidth)
    });
    
    let tl = new TimelineMax({ repeat: 1 })
      .to(element, 1, { x: "+=" + this.wrapWidth/*, rotationX: -rotationX*/ }, 0)
      .to(element, this.cellStep, { color: "#009688", scale: 1, repeat: 1, yoyo: true }, 0.5 - this.cellStep)
    
    this.baseTl.add(tl, index * -(this.cellStep));
  }


  updateProgress() {  
    let x: any = gsap.getProperty("#picker", "x");
    this.animation.progress(x / this.wrapWidth);
  }

  snapX(x) {
    return Math.round(x / this.cellWidth) * this.cellWidth;
  }











}
